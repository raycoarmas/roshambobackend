package com.ciklum.roshambo.totals;

import com.ciklum.roshambo.RoshamboApplicationTests;
import com.ciklum.roshambo.common.config.TotalMatchesWinnersStore;
import com.ciklum.roshambo.match.enums.ResultRoshamboMatchEnum;
import com.ciklum.roshambo.match.service.IRoshamboService;
import com.ciklum.roshambo.totals.dto.TotalMatchesWinnersDTO;
import com.ciklum.roshambo.totals.service.ITotalMatchesWinnersService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;
import java.util.Map;


class TotalMatchesWinnersTest extends RoshamboApplicationTests {

    @Autowired
    ITotalMatchesWinnersService totalMatchesWinnersService;

    @Autowired
    HttpSession httpSession;

    @Autowired
    IRoshamboService roshamboService;

    @Override
    public void config() {
    }

    @BeforeEach
    public void before(TestInfo testInfo){
        Map<ResultRoshamboMatchEnum, Integer> totals = TotalMatchesWinnersStore.getTotals();
        totals.put(ResultRoshamboMatchEnum.DRAW,0);
        totals.put(ResultRoshamboMatchEnum.PLAYER_ONE_WIN,0);
        totals.put(ResultRoshamboMatchEnum.PLAYER_TWO_WIN,0);
        testUtils.printInfo(testInfo.getDisplayName(),true);
    }


    @Test
    @Order(1)
    void getTotalsInitialization(){
        TotalMatchesWinnersDTO totalMatchesWinnersDTO = totalMatchesWinnersService.getTotalMatchWinners();
        Assertions.assertThat(totalMatchesWinnersDTO.getDraws()).isZero();
        Assertions.assertThat(totalMatchesWinnersDTO.getPlayerOneWins()).isZero();
        Assertions.assertThat(totalMatchesWinnersDTO.getPlayerTwoWins()).isZero();
        Assertions.assertThat(totalMatchesWinnersDTO.getTotalMatches()).isZero();
    }

    @Test
    @Order(2)
    void getTotalsAfterOneMatch(){
        roshamboService.playRoshamboMatch(httpSession);

        TotalMatchesWinnersDTO totalMatchesWinnersDTO = totalMatchesWinnersService.getTotalMatchWinners();

        Integer[] integers = {totalMatchesWinnersDTO.getDraws(),totalMatchesWinnersDTO.getPlayerOneWins(),totalMatchesWinnersDTO.getPlayerTwoWins()};

        Assertions.assertThat(integers).containsExactlyInAnyOrder(1,0,0);

        Assertions.assertThat(totalMatchesWinnersDTO.getTotalMatches()).isEqualTo(1);
    }

    @Test
    @Order(3)
    void getTotalsAfterNMatch(){
        for (int i = 0; i < 50; i++) {
            roshamboService.playRoshamboMatch(httpSession);
        }

        TotalMatchesWinnersDTO totalMatchesWinnersDTO = totalMatchesWinnersService.getTotalMatchWinners();

        Assertions.assertThat(totalMatchesWinnersDTO.getTotalMatches()).isEqualTo(50);
    }


}
