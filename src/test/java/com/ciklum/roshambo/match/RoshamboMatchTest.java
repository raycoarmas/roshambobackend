package com.ciklum.roshambo.match;

import com.ciklum.roshambo.RoshamboApplicationTests;
import com.ciklum.roshambo.common.constants.Constants;
import com.ciklum.roshambo.match.dto.RoshamboMatchDTO;
import com.ciklum.roshambo.match.enums.ResultRoshamboMatchEnum;
import com.ciklum.roshambo.match.enums.RoshamboHandEnum;
import com.ciklum.roshambo.match.service.IRoshamboService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

class RoshamboMatchTest extends RoshamboApplicationTests {

    @Override
    public void config() {
    }

    @Autowired
    HttpSession httpSession;

    @Autowired
    IRoshamboService roshamboService;

    @Test
    @Order(1)
    void getRoshamboHandTest(){
        List<RoshamboHandEnum> roshamboHandEnum = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            roshamboHandEnum.add(roshamboService.getRoshamboHand());
        }
        Assertions.assertThat(roshamboHandEnum).contains(RoshamboHandEnum.PAPER, RoshamboHandEnum.ROCK, RoshamboHandEnum.SCISSORS);
    }

    @Test
    @Order(2)
    void chooseMatchWinnerPlayerOnePaperTest(){
        ResultRoshamboMatchEnum resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.PAPER, RoshamboHandEnum.SCISSORS);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.PLAYER_TWO_WIN);

        resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.PAPER, RoshamboHandEnum.ROCK);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.PLAYER_ONE_WIN);

        resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.PAPER, RoshamboHandEnum.PAPER);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.DRAW);
    }

    @Test
    @Order(3)
    void chooseMatchWinnerPlayerOneScissorsTest(){
        ResultRoshamboMatchEnum resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.SCISSORS, RoshamboHandEnum.PAPER);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.PLAYER_ONE_WIN);

        resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.SCISSORS, RoshamboHandEnum.ROCK);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.PLAYER_TWO_WIN);

        resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.SCISSORS, RoshamboHandEnum.SCISSORS);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.DRAW);
    }

    @Test
    @Order(4)
    void chooseMatchWinnerPlayerOneRockTest() {
        ResultRoshamboMatchEnum resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.ROCK, RoshamboHandEnum.SCISSORS);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.PLAYER_ONE_WIN);

        resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.ROCK, RoshamboHandEnum.PAPER);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.PLAYER_TWO_WIN);

        resultRoshamboMatchEnum = roshamboService.chooseMatchWinner(RoshamboHandEnum.ROCK, RoshamboHandEnum.ROCK);
        Assertions.assertThat(resultRoshamboMatchEnum).isEqualTo(ResultRoshamboMatchEnum.DRAW);
    }

    @Test
    @Order(5)
    void playMatchAndGetHistoryTest(){
        ResponseEntity<RoshamboMatchDTO> responseEntity = roshamboService.playRoshamboMatch(httpSession);
        RoshamboMatchDTO roshamboMatchDTO = responseEntity.getBody();

        Assertions.assertThat(roshamboMatchDTO).isNotNull();
        Assertions.assertThat(roshamboMatchDTO.getPlayerOneChoose()).isIn(RoshamboHandEnum.PAPER, RoshamboHandEnum.ROCK, RoshamboHandEnum.SCISSORS);
        Assertions.assertThat(roshamboMatchDTO.getPlayerTwoChoose()).isEqualTo(RoshamboHandEnum.ROCK);
        Assertions.assertThat(roshamboMatchDTO.getPlayerWinner()).isIn(ResultRoshamboMatchEnum.DRAW, ResultRoshamboMatchEnum.PLAYER_ONE_WIN, ResultRoshamboMatchEnum.PLAYER_TWO_WIN);

        List<RoshamboMatchDTO> roshamboMatchDTOList = new ArrayList<>();
        roshamboMatchDTOList.add(roshamboMatchDTO);
        
        Assertions.assertThat(roshamboService.getHistory(httpSession)).isEqualTo(roshamboMatchDTOList);
    }

    @Test
    @Order(6)
    void removeHistory(){
       playMatchAndGetHistoryTest();
       roshamboService.deleteHistory(httpSession);
       Assertions.assertThat(roshamboService.getHistory(httpSession)).isEmpty();

    }
}
