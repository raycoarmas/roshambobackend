package com.ciklum.roshambo;

import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class RoshamboApplicationTests {

	private static final String START_CONF_MSG = "******** START TEST CONFIGURATION DATA*********";
	private static final String END_CONF_MSG = "******** END TEST CONFIGURATION DATA ********";
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	protected TestUtils testUtils;

	@BeforeEach
	public void before(TestInfo testInfo){
		testUtils.printInfo(testInfo.getDisplayName(),true);
	}

	@AfterEach
	public void after(TestInfo testInfo){
		testUtils.printInfo(testInfo.getDisplayName(),false);
	}

	/**
	 * Method with initial configuration for all test
	 */
	public abstract void config();

	@BeforeAll
	public void initialConfig() {
		log.info(START_CONF_MSG);
		config();
		log.info(END_CONF_MSG);

	}
}
