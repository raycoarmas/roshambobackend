package com.ciklum.roshambo.match.controller;

import com.ciklum.roshambo.common.exception.RoshamboException;
import com.ciklum.roshambo.match.dto.RoshamboMatchDTO;
import com.ciklum.roshambo.match.service.IRoshamboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
public class RoshamboMatchController {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    IRoshamboService roshamboService;

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/roshambo-match")
    public CompletableFuture<ResponseEntity<RoshamboMatchDTO>> playRoshamboMatch(HttpSession session){
        return CompletableFuture.supplyAsync(()->roshamboService.playRoshamboMatch(session))
                .exceptionally(throwable -> {
                    log.error(throwable.getMessage());
                    throw new RoshamboException("Error playing a Roshambo match");
                });
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/roshambo-history")
    public CompletableFuture<ResponseEntity<List<RoshamboMatchDTO>>> getHistory(HttpSession session){
        return CompletableFuture.supplyAsync(()-> new ResponseEntity<>(roshamboService.getHistory(session), HttpStatus.OK))
                .exceptionally(throwable -> {
                    log.error(throwable.getMessage());
                    throw new RoshamboException("Error recovering Roshambo history");
                });
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @DeleteMapping("/delete-roshambo-history")
    public CompletableFuture<Void> deleteHistory(HttpSession session){
        return CompletableFuture.runAsync(()-> roshamboService.deleteHistory(session))
                .exceptionally(throwable -> {
                    log.error(throwable.getMessage());
                    throw new RoshamboException("Error deleting Roshambo history");
                });
    }
}
