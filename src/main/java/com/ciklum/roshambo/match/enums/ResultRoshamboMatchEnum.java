package com.ciklum.roshambo.match.enums;

import com.ciklum.roshambo.common.exception.RoshamboException;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum ResultRoshamboMatchEnum {
    DRAW(0, "Draw"),
    PLAYER_ONE_WIN(1, "Player 1 win"),
    PLAYER_TWO_WIN(2, "Player 2 win");

    int value;
    String name;

    ResultRoshamboMatchEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public static ResultRoshamboMatchEnum getByValue(int value){
        return Arrays.stream(values())
                .filter(resultRoshamboMatchEnum -> resultRoshamboMatchEnum.getValue() == value)
                .findFirst()
                .orElseThrow(RoshamboException::new);

    }
}
