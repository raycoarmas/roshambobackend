package com.ciklum.roshambo.match.enums;

import com.ciklum.roshambo.common.exception.RoshamboException;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum RoshamboHandEnum {
    ROCK(0, "Rock"),
    PAPER(1, "Paper"),
    SCISSORS(2, "Scissors");

    int value;
    String name;

    RoshamboHandEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public static RoshamboHandEnum getByValue(int value){
        return Arrays.stream(values())
                .filter(roshamboHandEnum -> roshamboHandEnum.getValue() == value )
                .findFirst()
                .orElseThrow(RoshamboException::new);

    }

}
