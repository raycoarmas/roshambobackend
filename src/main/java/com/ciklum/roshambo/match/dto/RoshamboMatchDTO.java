package com.ciklum.roshambo.match.dto;

import com.ciklum.roshambo.match.enums.ResultRoshamboMatchEnum;
import com.ciklum.roshambo.match.enums.RoshamboHandEnum;
import lombok.Data;

@Data
public class RoshamboMatchDTO {

    RoshamboHandEnum playerOneChoose;
    RoshamboHandEnum playerTwoChoose;
    ResultRoshamboMatchEnum playerWinner;
}
