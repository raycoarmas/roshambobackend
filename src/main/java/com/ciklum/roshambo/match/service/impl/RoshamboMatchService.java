package com.ciklum.roshambo.match.service.impl;

import com.ciklum.roshambo.common.config.TotalMatchesWinnersStore;
import com.ciklum.roshambo.common.constants.Constants;
import com.ciklum.roshambo.common.utils.TransformUtils;
import com.ciklum.roshambo.match.dto.RoshamboMatchDTO;
import com.ciklum.roshambo.match.enums.ResultRoshamboMatchEnum;
import com.ciklum.roshambo.match.enums.RoshamboHandEnum;
import com.ciklum.roshambo.match.service.IRoshamboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class RoshamboMatchService implements IRoshamboService {

    @Autowired
    TotalMatchesWinnersStore totalMatchesWinnersStore;

    @Override
    public ResponseEntity<RoshamboMatchDTO> playRoshamboMatch(HttpSession session) {
        RoshamboMatchDTO roshamboMatchDTO = new RoshamboMatchDTO();
        roshamboMatchDTO.setPlayerOneChoose(getRoshamboHand());
        roshamboMatchDTO.setPlayerTwoChoose(RoshamboHandEnum.ROCK);
        roshamboMatchDTO.setPlayerWinner(chooseMatchWinner(roshamboMatchDTO.getPlayerOneChoose(), roshamboMatchDTO.getPlayerTwoChoose()));

        storeMatch(session, roshamboMatchDTO);
        totalMatchesWinnersStore.increaseTotals(roshamboMatchDTO.getPlayerWinner());

        return new ResponseEntity<>(roshamboMatchDTO, HttpStatus.OK);
    }

    @Override
    public RoshamboHandEnum getRoshamboHand() {
        return RoshamboHandEnum.getByValue(ThreadLocalRandom.current().nextInt(0,3));
    }

    @Override
    public ResultRoshamboMatchEnum chooseMatchWinner(RoshamboHandEnum playerOneChoose, RoshamboHandEnum playerTwoChoose) {
        return ResultRoshamboMatchEnum.getByValue((3 + playerOneChoose.getValue() - playerTwoChoose.getValue()) % 3);
    }

    @Override
    public List<RoshamboMatchDTO> getHistory(HttpSession session) {
        TransformUtils<RoshamboMatchDTO> transformUtils = new TransformUtils<>();
        String historyJson = getMatchHistory(session);
        return transformUtils.jsonToList(historyJson,RoshamboMatchDTO.class);
    }

    @Override
    public void deleteHistory(HttpSession session) {
        session.setAttribute(Constants.SESSION_HISTORY_KEY, "[]");
    }

    private void storeMatch(HttpSession session, RoshamboMatchDTO roshamboMatchDTO) {
        TransformUtils<RoshamboMatchDTO> transformUtils = new TransformUtils<>();

        String historyJson = getMatchHistory(session);
        List<RoshamboMatchDTO> roshamboMatchDTOList = transformUtils.jsonToList(historyJson,RoshamboMatchDTO.class);
        roshamboMatchDTOList.add(roshamboMatchDTO);
        session.setAttribute(Constants.SESSION_HISTORY_KEY, transformUtils.listToJson(roshamboMatchDTOList));
    }

    private String getMatchHistory(HttpSession session) {
        String historyJson = (String) session.getAttribute(Constants.SESSION_HISTORY_KEY);
        if (historyJson == null || historyJson.isEmpty()) {
            historyJson = "[]";
        }
        return historyJson;
    }
}
