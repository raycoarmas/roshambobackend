package com.ciklum.roshambo.match.service;

import com.ciklum.roshambo.match.dto.RoshamboMatchDTO;
import com.ciklum.roshambo.match.enums.ResultRoshamboMatchEnum;
import com.ciklum.roshambo.match.enums.RoshamboHandEnum;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface IRoshamboService {
    ResponseEntity<RoshamboMatchDTO> playRoshamboMatch(HttpSession session);
    RoshamboHandEnum getRoshamboHand();
    ResultRoshamboMatchEnum chooseMatchWinner(RoshamboHandEnum playerOneChoose, RoshamboHandEnum playerTwoChoose);
    List<RoshamboMatchDTO> getHistory(HttpSession session);
    void deleteHistory(HttpSession session);
}
