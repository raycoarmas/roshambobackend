package com.ciklum.roshambo.common.utils;

import com.ciklum.roshambo.common.exception.RoshamboException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class TransformUtils<T> {

    private final ObjectMapper objectMapper;

    public TransformUtils() {
        this.objectMapper = new ObjectMapper();
    }

    public String listToJson(List<T> object) {
        try {
            return this.objectMapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RoshamboException(e.getMessage());
        }
    }


    public List<T> jsonToList(String jsonString, Class<T> className) {
        try {
            return this.objectMapper.readValue(jsonString, this.objectMapper.getTypeFactory().constructCollectionType(List.class, className));
        } catch (IOException e) {
            throw new RoshamboException(e.getMessage());
        }
    }
}
