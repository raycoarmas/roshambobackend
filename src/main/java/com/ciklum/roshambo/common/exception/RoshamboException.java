package com.ciklum.roshambo.common.exception;

public class RoshamboException extends RuntimeException {
    public RoshamboException() {
    }

    public RoshamboException(String message) {
        super(message);
    }
}
