package com.ciklum.roshambo.common.config;

import com.ciklum.roshambo.match.enums.ResultRoshamboMatchEnum;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;
import java.util.Map;

@Configuration
public class TotalMatchesWinnersStore {

    private static final Map<ResultRoshamboMatchEnum, Integer> totals = new EnumMap<>(ResultRoshamboMatchEnum.class);

    public TotalMatchesWinnersStore() {
        totals.put(ResultRoshamboMatchEnum.DRAW,0);
        totals.put(ResultRoshamboMatchEnum.PLAYER_ONE_WIN,0);
        totals.put(ResultRoshamboMatchEnum.PLAYER_TWO_WIN,0);

    }

    public void increaseTotals(ResultRoshamboMatchEnum winner){
        Integer total = totals.get(winner);
        totals.put(winner, total+1);
    }

    public static Map<ResultRoshamboMatchEnum, Integer> getTotals() {
        return totals;
    }
}