package com.ciklum.roshambo.common.constants;

public class Constants {
    public static final String SESSION_HISTORY_KEY = "history";

    private Constants() {
        throw new IllegalStateException("Utility class");
    }
}
