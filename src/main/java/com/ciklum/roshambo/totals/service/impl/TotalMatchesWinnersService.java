package com.ciklum.roshambo.totals.service.impl;

import com.ciklum.roshambo.common.config.TotalMatchesWinnersStore;
import com.ciklum.roshambo.match.enums.ResultRoshamboMatchEnum;
import com.ciklum.roshambo.totals.dto.TotalMatchesWinnersDTO;
import com.ciklum.roshambo.totals.service.ITotalMatchesWinnersService;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicReference;

@Service
public class TotalMatchesWinnersService implements ITotalMatchesWinnersService {

    @Override
    public TotalMatchesWinnersDTO getTotalMatchWinners() {
        TotalMatchesWinnersDTO totalMatchesWinnersDTO = new TotalMatchesWinnersDTO();
        totalMatchesWinnersDTO.setDraws(TotalMatchesWinnersStore.getTotals().get(ResultRoshamboMatchEnum.DRAW));
        totalMatchesWinnersDTO.setPlayerOneWins(TotalMatchesWinnersStore.getTotals().get(ResultRoshamboMatchEnum.PLAYER_ONE_WIN));
        totalMatchesWinnersDTO.setPlayerTwoWins(TotalMatchesWinnersStore.getTotals().get(ResultRoshamboMatchEnum.PLAYER_TWO_WIN));

        AtomicReference<Integer> total = new AtomicReference<>(0);
        TotalMatchesWinnersStore.getTotals().forEach((resultRoshamboMatchEnum, integer) -> total.updateAndGet(v -> v + integer));

        totalMatchesWinnersDTO.setTotalMatches(total.get());

        return totalMatchesWinnersDTO;
    }
}
