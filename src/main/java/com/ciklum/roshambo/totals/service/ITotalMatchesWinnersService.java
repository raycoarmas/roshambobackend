package com.ciklum.roshambo.totals.service;

import com.ciklum.roshambo.totals.dto.TotalMatchesWinnersDTO;

public interface ITotalMatchesWinnersService {
    TotalMatchesWinnersDTO getTotalMatchWinners();
}
