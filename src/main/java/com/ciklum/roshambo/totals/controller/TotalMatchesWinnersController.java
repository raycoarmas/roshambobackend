package com.ciklum.roshambo.totals.controller;

import com.ciklum.roshambo.common.exception.RoshamboException;
import com.ciklum.roshambo.totals.dto.TotalMatchesWinnersDTO;
import com.ciklum.roshambo.totals.service.ITotalMatchesWinnersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.concurrent.CompletableFuture;

@RestController
public class TotalMatchesWinnersController {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ITotalMatchesWinnersService iTotalMatchesWinnersService;

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/total-matches-winners")
    public CompletableFuture<ResponseEntity<TotalMatchesWinnersDTO>> totalMatchWinners(HttpSession session){
        return CompletableFuture.supplyAsync(()-> new ResponseEntity<>(iTotalMatchesWinnersService.getTotalMatchWinners(), HttpStatus.OK))
                .exceptionally(throwable -> {
                    log.error(throwable.getMessage());
                    throw new RoshamboException("Error playing a Roshambo match");
                });
    }

}
