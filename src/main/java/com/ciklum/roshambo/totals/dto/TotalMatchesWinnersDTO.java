package com.ciklum.roshambo.totals.dto;

import lombok.Data;

@Data
public class TotalMatchesWinnersDTO {

    Integer playerOneWins;
    Integer playerTwoWins;
    Integer draws;
    Integer totalMatches;
}
